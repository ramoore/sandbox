package com.exactsciences.ramoore.sandbox;

import com.exactsciences.ramoore.sandbox.app.AuditDemo;

public class App {
    public static void main(String[] args) {
        AuditDemo.run(true);
    }
}
