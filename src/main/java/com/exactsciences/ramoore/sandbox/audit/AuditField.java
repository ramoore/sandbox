package com.exactsciences.ramoore.sandbox.audit;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AuditField {
    String value() default "";  // Handling in getAuditField() to use Java field name if empty string
}
