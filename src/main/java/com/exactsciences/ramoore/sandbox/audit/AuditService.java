package com.exactsciences.ramoore.sandbox.audit;

import java.util.List;

public class AuditService {
    // ============================================================
    // Fields
    // ============================================================

    private static AuditService instance;

    // ============================================================
    // Methods
    // ============================================================

    // ----------
    // public
    // ----------

    public synchronized static AuditService use() {
        if (instance == null) {
            instance = new AuditService();
        }

        return instance;
    }

    public void store(List<Diff> diffs) {
        // TODO: Add database storage? Simulate for now...
        for (Diff diff : diffs) {
            System.out.printf(" + %s\n", diff);
        }
    }
}
