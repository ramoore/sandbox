package com.exactsciences.ramoore.sandbox.audit;

public class Diff {
    // ============================================================
    // Fields
    // ============================================================

    private String moduleKey;
    private String classKey;
    private String id;
    private String field;
    private String oldValue;
    private String newValue;
    private String user;

    // ============================================================
    // Methods
    // ============================================================

    // ----------
    // public
    // ----------

    @Override
    public String toString() {
        String f = "%s [module: %s] [class: %s] [id: %s] [field: %s] [oldValue: %s] [newValue: %s] [user: %s]";
        return String.format(f, getClass().getSimpleName(), moduleKey, classKey, id, field, oldValue, newValue, user);
    }

    public String getModuleKey() {
        return moduleKey;
    }

    public void setModuleKey(String moduleKey) {
        this.moduleKey = moduleKey;
    }

    public String getClassKey() {
        return classKey;
    }

    public void setClassKey(String classKey) {
        this.classKey = classKey;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getOldValue() {
        return oldValue;
    }

    public void setOldValue(String oldValue) {
        this.oldValue = oldValue;
    }

    public String getNewValue() {
        return newValue;
    }

    public void setNewValue(String newValue) {
        this.newValue = newValue;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
