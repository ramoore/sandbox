package com.exactsciences.ramoore.sandbox.audit;

import java.lang.reflect.Field;
import java.util.*;

public class Audit<T> {
    // ============================================================
    // Fields
    // ============================================================

    private final String user;

    private String moduleKey;
    private String classKey;
    private String id;

    private Map<Field, String[]> kv;  // Key and array of size 3 for "key", "oldValue", and "newValue"

    // ============================================================
    // Methods
    // ============================================================

    // ----------
    // public
    // ----------

    public Audit(T before, String user) {
        if (before == null) {
            throw new RuntimeException("Cannot initialize audit with null object");
        }

        this.user = user;
        this.kv = new HashMap<>();

        try {
            init(before);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Diff> execute(T after) {
        if (after == null) {
            throw new RuntimeException("Cannot execute audit with null object");
        }

        try {
            populate(after);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        return compare();
    }

    // ----------
    // private
    // ----------

    private void init(T before) throws IllegalAccessException {
        Class<?> cls = before.getClass();

        AuditModule am = cls.getAnnotation(AuditModule.class);
        if (am == null) {
            String e = String.format("No AuditModule annotation found for %s", cls.getName());
            throw new RuntimeException(e);
        } else {
            this.moduleKey = am.value();
        }

        AuditClass ac = cls.getAnnotation(AuditClass.class);
        if (ac == null) {
            String e = String.format("No AuditClass annotation found for %s", cls.getName());
            throw new RuntimeException(e);
        } else {
            this.classKey = ac.value();
        }

        for (Field f : cls.getDeclaredFields()) {
            if (f.isAnnotationPresent(AuditIdentifier.class)) {
                f.setAccessible(true);
                this.id = f.get(before).toString();
            }

            if (f.isAnnotationPresent(AuditField.class)) {
                f.setAccessible(true);
                String field = getAuditField(f);
                String value = getAuditValue(f, before);
                String[] comparison = new String[]{field, value, null};  // 0 = "key", 1 = "oldValue, 2 = "newValue"
                kv.put(f, comparison);
            }
        }

        if (this.id == null) {
            String e = String.format("No AuditIdentifier annotation found for %s", cls.getName());
            throw new RuntimeException(e);
        }

        if (this.kv.size() == 0) {
            String e = String.format("No AuditField annotations found for %s", cls.getName());
            throw new RuntimeException(e);
        }
    }

    private void populate(T after) throws IllegalAccessException {
        for (Field f : kv.keySet()) {
            kv.get(f)[2] = getAuditValue(f, after);
        }
    }

    private List<Diff> compare() {
        List<Diff> diffs = new ArrayList<>();

        for (Field f : kv.keySet()) {
            String field = kv.get(f)[0];
            String oldValue = kv.get(f)[1];
            String newValue = kv.get(f)[2];

            boolean isIdentical;
            if (oldValue == null) {
                isIdentical = (newValue == null);
            } else {
                isIdentical = oldValue.equals(newValue);
            }

            if (!isIdentical) {
                Diff diff = new Diff();
                diff.setModuleKey(moduleKey);
                diff.setClassKey(classKey);
                diff.setId(id);
                diff.setField(field);
                diff.setOldValue(oldValue);
                diff.setNewValue(newValue);
                diff.setUser(user);
                diffs.add(diff);
            }
        }

        return diffs;
    }

    private String getAuditField(Field f) {
        AuditField af = f.getAnnotation(AuditField.class);

        return af.value().isEmpty() ? f.getName() : af.value();  // Use Java field name if no annotation value
    }

    private String getAuditValue(Field f, T object) throws IllegalAccessException {
        if (f.get(object) == null) {
            return null;
        }

        return f.get(object).toString();
    }
}
