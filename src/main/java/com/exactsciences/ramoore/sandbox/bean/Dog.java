package com.exactsciences.ramoore.sandbox.bean;

import com.exactsciences.ramoore.sandbox.audit.AuditField;
import com.exactsciences.ramoore.sandbox.audit.AuditIdentifier;
import com.exactsciences.ramoore.sandbox.audit.AuditClass;
import com.exactsciences.ramoore.sandbox.audit.AuditModule;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@AuditModule("pets")
@AuditClass("doggo")
public class Dog {
    @AuditIdentifier
    private UUID id;
    private String name;
    @AuditField("years")
    private int age;
    private boolean goodBoy;
    @AuditField
    private Instant lastIncident;

    public Dog() {
        this.id = UUID.randomUUID();
    }

    public Dog(String name, int age, boolean goodBoy) {
        this();
        this.name = name;
        this.age = age;
        this.goodBoy = goodBoy;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isGoodBoy() {
        return goodBoy;
    }

    public void setGoodBoy(boolean goodBoy) {
        this.goodBoy = goodBoy;
    }

    public Instant getLastIncident() {
        return lastIncident;
    }

    public void setLastIncident(Instant lastIncident) {
        this.lastIncident = lastIncident;
    }
}
