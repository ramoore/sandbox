package com.exactsciences.ramoore.sandbox.app;

import com.exactsciences.ramoore.sandbox.audit.Audit;
import com.exactsciences.ramoore.sandbox.audit.AuditService;
import com.exactsciences.ramoore.sandbox.bean.Dog;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class AuditDemo {
    // ============================================================
    // Fields
    // ============================================================

    private static List<Double> timestamps = new ArrayList<>();

    // ============================================================
    // Methods
    // ============================================================

    // ----------
    // public
    // ----------

    public static void run(boolean verbose) {
        // Create dog - i.e., simulate "pre-change" database object
        Dog dog = new Dog("Dash", 4, true);

        // Initialize audit (with "ts" timestamps)
        ts();
        Audit<Dog> audit = new Audit<>(dog, "ramoore");  // *** START AUDIT ***
        ts();

        // Update dog - i.e., simulate user changes
        dog.setAge(dog.getAge() + 1);  // Dash turned 5 years old! (audited)
        dog.setGoodBoy(false);  // He celebrated by peeing on the rug... (not audited)
        dog.setLastIncident(Instant.now());  // He celebrated by peeing on the rug... (audited)

        // Execute audit (with "ts" timestamps)
        ts();
        AuditService.use().store(audit.execute(dog));  // *** FINISH AUDIT ***
        ts();

        // Handle 'verbose' mode
        if (timestamps.size() > 0) {
            System.out.printf("Audit took %f seconds to initialize\n", (timestamps.get(1) - timestamps.get(0)));
            System.out.printf("Audit took %f seconds to execute\n", (timestamps.get(3) - timestamps.get(2)));
        }
    }

    // ----------
    // private
    // ----------

    private static void ts() {
        timestamps.add(Instant.now().getNano() / (double) 1000000000);
    }

    // Simple example without timestamp logging for performance metrics
    private static void example() {
        // Create dog - i.e., simulate "pre-change" database object
        Dog dog = new Dog("Dash", 4, true);

        // Initialize audit
        Audit<Dog> audit = new Audit<>(dog, "ramoore");  // *** START AUDIT ***

        // Update dog - i.e., simulate user changes
        dog.setAge(dog.getAge() + 1);  // Dash turned 5 years old! (audited)
        dog.setGoodBoy(false);  // He celebrated by peeing on the rug... (not audited)
        dog.setLastIncident(Instant.now());  // He celebrated by peeing on the rug... (audited)

        // Execute audit
        AuditService.use().store(audit.execute(dog));  // *** FINISH AUDIT ***
    }
}
